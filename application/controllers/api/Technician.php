<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ini_set('max_execution_time', 120);
ini_set('memory_limit','512M'); 
ini_set('sqlsrv.ClientBufferMaxKBSize','524288');
ini_set('pdo_sqlsrv.client_buffer_max_kb_size','524288');

class Technician extends CI_Controller {
	function __construct()
	{
		parent::__construct();
        $this->load->model('m_technician');
	} 

	
	function getSubscriber(){
        $rs = $this->m_technician->getListSubscriber();
		echo json_encode(array('data' => $rs));
	}
	
	function updateTechnician(){
		if(isset($_POST['partner_id'])){
			$id = $_POST['partner_id'];
			$obj = array(
				'technician_date' => date('Y-m-d H:i:s'),
				'technician_status' => 1
			);
			$rs = $this->m_technician->updateTechnician($id, $obj);
			echo json_encode(array('status' => $rs));
		}else {
			echo json_encode(array('status' => 'Gak Ada Param'));
			
		}
	}
}