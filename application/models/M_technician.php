<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class M_technician extends CI_Model{
    function getListSubscriber(){
        $this->db->select('a.partner_id,a.full_name,a.address,a.phone');
        $this->db->join('m_partner_product b','b.partner_id=a.partner_id');
        $this->db->where('b.date_activation IS NULL');
        $this->db->where('b.date_dismantle IS NULL');
        $this->db->where('a.is_subscriber',1);
        $this->db->where('a.is_active',1);
        $this->db->where('b.technician_status',0);
        $this->db->order_by('b.date_created','desc');
		$query = $this->db->get('m_partner a');
		return $query->result();
    }
    function updateTechnician($id,$obj){
        $this->db->where('partner_id', $id);
        $this->db->update('m_partner_product', $obj);
        return $this->db->affected_rows();
    }
}
