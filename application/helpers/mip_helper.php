<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
function checkLogin() {
    $CI =& get_instance();

	$obj = $CI->session->userdata('admin');
	if (!$obj) header ("Location:".base_url('Welcome/login'));

	return $obj; 	
}


function checkAccess($obj, $menu, $allow, $remark = null){
    $CI =& get_instance();
    $url = base_url($CI->router->fetch_class());
    if ($obj->$allow == 0){
        print '<script>alert("You have no access right!")</script>';
        print '<script>window.location = "'. $url .'"</script>';
    
    }else {
        $user_access = json_decode($obj->access, TRUE);
        foreach ($user_access as $ua):
            if ($ua['menu_name'] == $menu) $access_id = $ua['access_id'];
        endforeach;
        if ($access_id){
            $data = array(
                'access_id' => $access_id,
                'remark' => $remark
            );
            $CI->load->model('m_log');
            $CI->m_log->insertUserLog($data);
            
        }else {
            die('<h3>You have no access right!</h3>');
        }
    }

}

/**
 * Function: sanitize
 * Returns a sanitized string, typically for URLs.
 *
 * Parameters:
 *     $string - The string to sanitize.
 *     $force_lowercase - Force the string to lowercase?
 *     $anal - If set to *true*, will remove all non-alphanumeric characters.
 */
function sanitize($string, $force_lowercase = true, $anal = false) {
    $strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]",
                   "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
                   "â€”", "â€“", ",", "<", ">", "/", "?");
    $clean = trim(str_replace($strip, "", strip_tags($string)));
    $clean = preg_replace('/\s+/', "-", $clean);
    $clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean ;
    return ($force_lowercase) ?
        (function_exists('mb_strtolower')) ?
            mb_strtolower($clean, 'UTF-8') :
            strtolower($clean) :
        $clean;
}

function number_count($number) {
    $units = array( '', 'K', 'M', 'B');
    $power = $number > 0 ? floor(log($number, 1000)) : 0;
    if($power > 0)
        return @number_format($number / pow(1000, $power), 2, '.', ' ').' '.$units[$power];
    else
        return @number_format($number / pow(1000, $power), 0, '', '');
}