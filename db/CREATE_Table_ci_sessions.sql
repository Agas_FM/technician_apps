USE [agti]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ci_sessions](
	[id] [varchar](128) NOT NULL,
	[ip_address] [varchar](45) NOT NULL,
	[timestamp] [bigint] NOT NULL,
	[data] [varchar](max) NOT NULL,
	[date_created] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[ci_sessions] ADD  DEFAULT ((0)) FOR [timestamp]
GO

ALTER TABLE [dbo].[ci_sessions] ADD  DEFAULT ('') FOR [data]
GO

ALTER TABLE [dbo].[ci_sessions] ADD  DEFAULT (getdate()) FOR [date_created]
GO


