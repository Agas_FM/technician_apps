USE [agti]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[apps_change_password]
	@wa varchar(50),
	@password varchar(50)
AS
BEGIN
	SET NOCOUNT ON;
	
	UPDATE t_partner_apps
		SET password=@password
	WHERE whatsapp=@wa

	SELECT
		a.partner_id,
		a.whatsapp,
		a.password,
		a.token,
		a.full_name,
		b.email,
		b.address
	
	FROM t_partner_apps a
	JOIN m_partner b ON b.partner_id=a.partner_id
	WHERE a.is_active=1 AND b.is_active=1
		AND a.whatsapp=@wa
		AND a.password=@password
		
END
