USE [agti]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[apps_set_register]
	@partner_id bigint,
	@full_name varchar(200),
	@wa varchar(50),
	@email varchar(100),
	@password varchar(50),
	@imei varchar(50),
	@device varchar(100)
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	BEGIN TRY
		DECLARE @tblResult TABLE(Id bigint, ProcessNumber varchar(100), ProcessMessage varchar(max));
		DECLARE @user_id int
		DECLARE @current_email varchar(100)
		DECLARE @token varchar(6)
		DECLARE @is_active int

	BEGIN TRANSACTION;

		
			
		SELECT TOP 1 @current_email=email FROM m_partner WHERE partner_id=@partner_id

		IF @current_email IS NOT NULL 
			BEGIN
				IF @current_email <> @email  
					BEGIN
						INSERT INTO @tblResult 
						SELECT 
							'0' AS Id,
							'2' AS ProcessNumber,
							'Email tidak terdaftar disistem kami!'  AS ProcessMessage
					END
				ELSE
					BEGIN
						SELECT @token=dbo.fc_get_token()
						SELECT TOP 1 @user_id=[user_id],@is_active=is_active FROM t_partner_apps WHERE whatsapp=@wa
						
						IF @user_id IS NULL
							BEGIN 
								INSERT INTO t_partner_apps (partner_id,full_name,whatsapp,password,token,imei,device_name,is_active)
									VALUES (@partner_id,@full_name,@wa,@password,@token,@imei,@device,0)

								SELECT @user_id=SCOPE_IDENTITY()

							END
						ELSE
							BEGIN
								UPDATE t_partner_apps 
									SET token=@token,
									imei=@imei,
									device_name=@device,
									password=@password,
									full_name=@full_name
								WHERE user_id=@user_id

							END
							
						INSERT INTO t_notif_queued (notif_id,[to],subject,body,status_data)
									VALUES (3,@wa,'Verifikasi Registrasi','Kode Verifikasi: ' + @token,1) 

						INSERT INTO @tblResult 
						SELECT 
							@user_id AS Id,
							'2' AS ProcessNumber,
							'Proses selanjutnya masukkan token' AS ProcessMessage
					END
			END
		ELSE
			BEGIN
				INSERT INTO @tblResult 
				SELECT 
					'0' AS Id,
					'2' AS ProcessNumber,
					'ID Pelanggan tidak dikenal!'  AS ProcessMessage
			END
		
	


		COMMIT TRANSACTION;

		SELECT * FROM @tblResult 

	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION; 
		SELECT  
			0 AS Id
			,ERROR_NUMBER() AS ProcessNumber
			,ERROR_MESSAGE() AS ProcessMessage  
			,ERROR_SEVERITY() AS ErrorSeverity  
			,ERROR_STATE() AS ErrorState  
			,ERROR_PROCEDURE() AS ErrorProcedure  
			,ERROR_LINE() AS ErrorLine;  
	END CATCH;
END
