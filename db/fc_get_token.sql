USE [agti] 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fc_get_token] ()
RETURNS varchar(6)
AS
BEGIN
	DECLARE @token varchar(6)
	DECLARE @i int
	SET @i = 1
	WHILE @i = 1
		BEGIN
			SELECT @token=RIGHT(token_result,6) FROM token_view
			IF NOT EXISTS(SELECT TOP 1 1 FROM t_partner_apps WHERE token=@token)
				BEGIN
					SET @i = 0
				END
		END
		

	RETURN @token

END
