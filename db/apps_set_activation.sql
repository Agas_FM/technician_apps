USE [agti]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[apps_set_activation]
	@partner_id bigint,
	@wa varchar(50),
	@imei varchar(50),
	@token varchar(6),
	@password varchar(50)

AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	BEGIN TRY
		DECLARE @tblResult TABLE(Id bigint, ProcessNumber varchar(100), ProcessMessage varchar(max));
		
		DECLARE @user_id int
		
	BEGIN TRANSACTION;

		SELECT TOP 1 @user_id=[user_id] FROM t_partner_apps 
			WHERE partner_id=@partner_id AND whatsapp=@wa AND token=@token
			AND imei=@imei AND password=@password

		IF @user_id IS NULL 
			BEGIN
				INSERT INTO @tblResult 
				SELECT 
					'0' AS Id,
					'1' AS ProcessNumber,
					'Kode Aktifasi salah!' AS ProcessMessage
						
			END
		ELSE 
			BEGIN
				UPDATE t_partner_apps
					SET is_active=1, date_updated=GETDATE()
				WHERE user_id=@user_id

				INSERT INTO @tblResult 
				SELECT 
					@user_id AS Id,
					'1' AS ProcessNumber,
					'Aktifasi berhasil!'  AS ProcessMessage
			END
		
	

		COMMIT TRANSACTION;

		SELECT * FROM @tblResult 

	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION; 
		SELECT  
			0 AS Id
			,ERROR_NUMBER() AS ProcessNumber
			,ERROR_MESSAGE() AS ProcessMessage  
			,ERROR_SEVERITY() AS ErrorSeverity  
			,ERROR_STATE() AS ErrorState  
			,ERROR_PROCEDURE() AS ErrorProcedure  
			,ERROR_LINE() AS ErrorLine;  
	END CATCH;
END
