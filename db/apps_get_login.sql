USE [agti]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[apps_get_login]
	@partner_id bigint,
	@wa varchar(50),
	@password varchar(50),
	@token varchar(6)
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		a.partner_id,
		a.whatsapp,
		a.password,
		a.token,
		a.full_name,
		b.email,
		b.address,
		b.is_subscriber

	FROM t_partner_apps a
	JOIN m_partner b ON b.partner_id=a.partner_id
	WHERE a.is_active=1 AND b.is_active=1
		AND a.partner_id=@partner_id
		AND a.whatsapp=@wa
		AND a.password=@password
		AND a.token=@token
		
END
